import imaplib
from Adafruit_PWM_Servo_Driver import PWM
import time
import picamera
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
import bluetooth

pwm = PWM(0x40)
servoMin = 150
servoMax = 600

def who_home():
    result = ""
    ac_result = bluetooth.lookup_name('78:F8:82:55:B0:46', timeout=5)
    if (ac_result != None):
        result += "Aaron C.: in\n"
    else:
        result += "Aaron C.: out\n"
        
    ay_result = bluetooth.lookup_name('C0:EE:FB:5A:78:6E', timeout=5)
    if (ay_result != None):
        result += "Aaron Y.: in\n"
    else:
       result += "Aaron Y.: out\n"
        
    d_result = bluetooth.lookup_name('2C:8A:72:17:F5:C1', timeout=5)
    if (d_result != None):
        result += "Derek: in\n"
    else:
        result += "Derek: out\n"
        
    z_result = bluetooth.lookup_name('A0:ED:CD:95:45:1F', timeout=5)
    if (z_result != None):
        result += "Zenas: in"
    else:
       result += "Zenas: out"

    return result

def Send_Plain_Mail(subject,message):
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = 'yc3lock@gmail.com'
    msg['To'] = 'yeeab@uci.edu'
    text = MIMEText(message)
    msg.attach(text)

    s = smtplib.SMTP('smtp.gmail.com',587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('yc3lock@gmail.com', 'smartlock3')
    s.sendmail('yc3lock@gmail.com', 'yeeab@uci.edu', msg.as_string())
    s.quit()    
def SendMail(ImgFileName):
    img_data = open(ImgFileName, 'rb').read()
    msg = MIMEMultipart()
    msg['Subject'] = 'Snapshot at Door'
    msg['From'] = 'yc3lock@gmail.com'
    msg['To'] = 'yeeab@uci.edu'

    text = MIMEText("This is what is at your door.")
    msg.attach(text)
    image = MIMEImage(img_data, name=os.path.basename(ImgFileName))
    msg.attach(image)

    s = smtplib.SMTP('smtp.gmail.com',587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('yc3lock@gmail.com', 'smartlock3')
    s.sendmail('yc3lock@gmail.com', 'yeeab@uci.edu', msg.as_string())
    s.quit()

def process_email(username, password):
    # Login to INBOX
    imap = imaplib.IMAP4_SSL("imap.gmail.com", 993)
    imap.login(username, password)
    imap.select('INBOX')
    status, response = imap.search('INBOX', '(UNSEEN)')
    unread_msg_nums = response[0].split()

    # Print all unread messages from a certain sender of interest
    status, response = imap.search(None, '(UNSEEN)')
    unread_msg_nums = response[0].split()
    for e_id in unread_msg_nums:
        _, response = imap.fetch(e_id, '(UID BODY[TEXT])')
        command = str(response[0][1])
        if ("Open the Door" in command):
            pwm.setPWM(0, 0, servoMin)
            time.sleep(1)
            Send_Plain_Mail("Your door has been unlocked.","Please take action if you were unaware of this."):
        elif ("Lock the Door" in command):
            pwm.setPWM(0, 0, servoMax)
            time.sleep(1)
        elif ("Take Snapshot" in command):
            with picamera.PiCamera() as camera:
                camera.capture('image.jpg')
                SendMail("image.jpg")
        elif ("Who is Home" in command):
            Send_Plain_Mail("This is Who is Home",who_home())

    # Mark them as seen
    for e_id in unread_msg_nums:
        imap.store(e_id, '+FLAGS', '\Seen')
    imap.close()
    imap.logout()

while True:
    process_email('YC3lock','smartlock3')
    
